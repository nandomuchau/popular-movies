package com.muchau.popularmovies;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.muchau.popularmovies.adapter.MovieAdapter;
import com.muchau.popularmovies.database.AppDatabase;
import com.muchau.popularmovies.model.Movie;
import com.muchau.popularmovies.sync.MovieAsyncTask;
import com.muchau.popularmovies.utilities.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis F. Muchau on 5/8/2018.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String KEY_INSTANCE_STATE_RV_POSITION = "grid_position";

    public static SharedPreferences sharedpreferences;

    private static final String myPreference = "myPreferences";

    public static final String sortBy = "sortBy";

    public static final String MY_FAVORITE = "my_favorite";

    private MovieAdapter movieAdapter;

    private RecyclerView recyclerView;

    private Parcelable savedRecyclerLayoutState;

    private AppDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mDb = AppDatabase.getInstance(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedpreferences = getSharedPreferences(myPreference,
                Context.MODE_PRIVATE);

        recyclerView = findViewById(R.id.recycler_view_movies);
        int numberOfColumns = calculateNoOfColumns(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        movieAdapter = new MovieAdapter(this, new ArrayList<Movie>());
        recyclerView.setAdapter(movieAdapter);
        loadMovies();
    }

    private void loadMovies(){
        if (MainActivity.sharedpreferences.contains(MainActivity.sortBy) &&
                MainActivity.sharedpreferences.getString(MainActivity.sortBy,"")
                        .equals(MainActivity.MY_FAVORITE)) {
            mDb.favoriteMovieDao().getFavoritedMovies().observe(this, new Observer<List<Movie>>() {
                @Override
                public void onChanged(@Nullable List<Movie> movies) {
                    movieAdapter.updateMovies((ArrayList<Movie>) movies);
                    movieAdapter.notifyDataSetChanged();
                }
            });
        } else {
            if (NetworkUtils.isInternetAvailable(this)) {
                new MovieAsyncTask(this, movieAdapter) {
                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        if (savedRecyclerLayoutState != null) {
                            recyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
                        }
                    }
                }.execute();
            } else {
                Toast.makeText(this, R.string.no_internet, Toast.LENGTH_LONG).show();
            }
        }
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int scalingFactor = 200;
        int noOfColumns = (int) (dpWidth / scalingFactor);
        if(noOfColumns < 2)
            noOfColumns = 2;
        return noOfColumns;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        SharedPreferences.Editor editor = sharedpreferences.edit();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_my_favorites) {
            editor.putString(sortBy, MY_FAVORITE);
            editor.apply();
            loadMovies();
            return true;
        } else if (id == R.id.action_most_popular) {
            editor.putString(sortBy, NetworkUtils.POPULAR_SORT);
            editor.apply();
            loadMovies();
            return true;
        } else if (id == R.id.action_highest_rated) {
            editor.putString(sortBy, NetworkUtils.TOP_RATED_SORT);
            editor.apply();
            loadMovies();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //restore recycler view at same position
        if (savedInstanceState != null) {
            savedRecyclerLayoutState = savedInstanceState.getParcelable(KEY_INSTANCE_STATE_RV_POSITION);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_INSTANCE_STATE_RV_POSITION, recyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    public void loadMovieDetailsActivity(Movie movie) {
        Intent intent = new Intent(this, MovieActivity.class);
        intent.putExtra("movie", movie);
        startActivity(intent);
    }
}
