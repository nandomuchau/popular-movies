package com.muchau.popularmovies;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.muchau.popularmovies.adapter.ReviewAdapter;
import com.muchau.popularmovies.adapter.TrailerAdapter;
import com.muchau.popularmovies.database.AppDatabase;
import com.muchau.popularmovies.model.Movie;
import com.muchau.popularmovies.model.Review;
import com.muchau.popularmovies.model.Trailer;
import com.muchau.popularmovies.sync.ReviewAsyncTask;
import com.muchau.popularmovies.sync.TrailerAsyncTask;
import com.muchau.popularmovies.utilities.DateUtils;
import com.muchau.popularmovies.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieActivity extends AppCompatActivity {

    private static final String PICTURE_URL = "http://image.tmdb.org/t/p/";

    private static final String PICTURE_ORIGINAL = "original";

    private static final String PICTURE_W185 = "w185";

    private Movie movie;

    private TrailerAdapter trailerAdapter;

    private ReviewAdapter reviewAdapter;

    private AppDatabase mDb;

    private ImageButton favoriteIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        mDb = AppDatabase.getInstance(getApplicationContext());

        movie = (Movie) getIntent().getSerializableExtra("movie");

        ImageView background = findViewById(R.id.image_view_backdrop_path);

        ImageView poster = findViewById(R.id.image_view_poster_path);

        String pictureUrl = PICTURE_URL + PICTURE_ORIGINAL + movie.getBackdropPath();

        String posterUrl = PICTURE_URL + PICTURE_W185 + movie.getPosterPath();

        favoriteIcon = findViewById(R.id.favorite_imageButton);
        changeFavoriteIcon();

        Picasso.with(this)
                .load(pictureUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .into(background);

        Picasso.with(this)
                .load(posterUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .into(poster);

        TextView movieTitle = findViewById(R.id.title_text_view);
        movieTitle.setText(movie.getOriginalTitle());

        TextView releaseDate = findViewById(R.id.release_date_text_view);
        releaseDate.setText(DateUtils.formatDate("yyyy-MM-dd","MM/dd/yyyy",
                movie.getReleaseDate()));

        RatingBar voteAverageBar = findViewById(R.id.vote_average_rating_bar);
        voteAverageBar.setMax(10);
        voteAverageBar.setMin(0);
        voteAverageBar.setRating(movie.getVoteAverage()/2);

        TextView description = findViewById(R.id.description_text_view);
        description.setText(movie.getOverview());

        RecyclerView trailerRecyclerView = findViewById(R.id.recycler_view_trailer);
        trailerRecyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        trailerAdapter = new TrailerAdapter(this, new ArrayList<Trailer>());
        trailerRecyclerView.setAdapter(trailerAdapter);

        RecyclerView reviewRecyclerView = findViewById(R.id.recycler_view_review);
        reviewRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        reviewAdapter = new ReviewAdapter(this, new ArrayList<Review>());
        reviewRecyclerView.setAdapter(reviewAdapter);

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(reviewRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        reviewRecyclerView.addItemDecoration(mDividerItemDecoration);

        loadTrailers();
        loadReviews();
    }

    private void changeFavoriteIcon() {
        if(movie != null && movie.getId() > 0) {
            favoriteIcon.setImageDrawable(this.getDrawable(R.drawable.ic_favorite_24px));
        } else {
            favoriteIcon.setImageDrawable(this.getDrawable(R.drawable.ic_favorite_border_24px));
        }
    }

    private void loadTrailers(){
        if (NetworkUtils.isInternetAvailable(this)) {
            new TrailerAsyncTask(this, trailerAdapter, movie.getMovieId()).execute();
        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_LONG).show();
        }
    }

    private void loadReviews(){
        if (NetworkUtils.isInternetAvailable(this)) {
            new ReviewAsyncTask(this, reviewAdapter, movie.getMovieId()).execute();
        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_LONG).show();
        }
    }

    public void addFavorite(View view) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if(movie != null && movie.getId() > 0) {
                    mDb.favoriteMovieDao().deleteFavoriteMovie(movie);
                } else {
                    mDb.favoriteMovieDao().insertFavoriteMovie(movie);
                }
                Movie favorite = mDb.favoriteMovieDao().loadFavoriteMovieByMovieId(movie.getMovieId());
                movie.setId(favorite == null ? 0 : favorite.getId());
                changeFavoriteIcon();
            }
        });
    }
}
