package com.muchau.popularmovies.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.muchau.popularmovies.model.Movie;

import java.util.List;

/**
 * Created by Luis F. Muchau on 6/22/2018.
 */
@Dao
public interface FavoriteMovieDao {

    @Query("SELECT * FROM favorite_movie ORDER BY id")
    LiveData<List<Movie>> getFavoritedMovies();

    @Query("SELECT * FROM favorite_movie ORDER BY id")
    List<Movie> loadAllFavoriteMovies();

    @Insert
    void insertFavoriteMovie(Movie movie);

    @Delete
    void deleteFavoriteMovie(Movie movie);

    @Query("SELECT * FROM favorite_movie WHERE movieID = :movieId")
    Movie loadFavoriteMovieByMovieId(int movieId);
}
