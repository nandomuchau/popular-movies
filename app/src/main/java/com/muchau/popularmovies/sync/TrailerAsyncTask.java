package com.muchau.popularmovies.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.muchau.popularmovies.utilities.JsonUtils;
import com.muchau.popularmovies.adapter.TrailerAdapter;
import com.muchau.popularmovies.model.Trailer;
import com.muchau.popularmovies.utilities.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 6/20/2018.
 */
public class TrailerAsyncTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    private int movieId;

    private TrailerAdapter trailerAdapter;

    private ArrayList<Trailer> trailers;

    private static final String TAG = TrailerAsyncTask.class.getSimpleName();

    public TrailerAsyncTask(Context context, TrailerAdapter trailerAdapter, int movieId){
        this.context = context;
        this.trailerAdapter = trailerAdapter;
        this.movieId = movieId;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL requestUrl = NetworkUtils.getTrailerUrl(this.context, movieId);
            String jsonResponse = NetworkUtils.getResponseFromHttpUrl(requestUrl);
            /* Parse the JSON into a list of weather values */
            this.trailers = JsonUtils.getTrailerValuesFromJson(jsonResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        trailerAdapter.updateTrailers(this.trailers);
        trailerAdapter.notifyDataSetChanged();
    }
}