package com.muchau.popularmovies.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.muchau.popularmovies.adapter.ReviewAdapter;
import com.muchau.popularmovies.model.Review;
import com.muchau.popularmovies.utilities.JsonUtils;
import com.muchau.popularmovies.utilities.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 6/21/2018.
 */
public class ReviewAsyncTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    private int movieId;

    private ReviewAdapter reviewAdapter;

    private ArrayList<Review> reviews;

    private static final String TAG = ReviewAsyncTask.class.getSimpleName();

    public ReviewAsyncTask(Context context, ReviewAdapter reviewAdapter, int movieId){
        this.context = context;
        this.reviewAdapter = reviewAdapter;
        this.movieId = movieId;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL requestUrl = NetworkUtils.getReviewUrl(this.context, movieId);
            String jsonResponse = NetworkUtils.getResponseFromHttpUrl(requestUrl);
            /* Parse the JSON into a list of weather values */
            this.reviews = JsonUtils.getReviewValuesFromJson(jsonResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        reviewAdapter.updateReviews(this.reviews);
        reviewAdapter.notifyDataSetChanged();
    }
}