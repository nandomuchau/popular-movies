package com.muchau.popularmovies.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.muchau.popularmovies.database.AppDatabase;
import com.muchau.popularmovies.utilities.JsonUtils;
import com.muchau.popularmovies.adapter.MovieAdapter;
import com.muchau.popularmovies.model.Movie;
import com.muchau.popularmovies.utilities.NetworkUtils;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis F. Muchau on 5/9/2018.
 */
public class MovieAsyncTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    private MovieAdapter movieAdapter;

    private ArrayList<Movie> movies;

    private AppDatabase mDb;

    private static final String TAG = MovieAsyncTask.class.getSimpleName();

    public MovieAsyncTask(Context context, MovieAdapter movieAdapter){
        this.context = context;
        this.movieAdapter = movieAdapter;
        this.mDb = AppDatabase.getInstance(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            List<Movie> favoriteMovies = mDb.favoriteMovieDao().loadAllFavoriteMovies();

            URL requestUrl = NetworkUtils.getUrl(this.context);
            String jsonResponse = NetworkUtils.getResponseFromHttpUrl(requestUrl);
            /* Parse the JSON into a list of weather values */
            this.movies = JsonUtils.getMovieValuesFromJson(jsonResponse);

            // Update movie id if the movie is in the favorite list
            for (Movie movie : movies) {
                for (Movie favorite : favoriteMovies) {
                    if (movie.getMovieId() == favorite.getMovieId()) {
                        movie.setId(favorite.getId());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        movieAdapter.updateMovies(this.movies);
        movieAdapter.notifyDataSetChanged();
    }
}