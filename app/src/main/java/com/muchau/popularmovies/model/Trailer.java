package com.muchau.popularmovies.model;

/**
 * Created by Luis F. Muchau on 6/20/2018.
 */
public class Trailer {

    private int movieId;

    private String id;

    private String key;

    private String name;

    private String site;

    private int size;

    private String type;

    public Trailer() {
        super();
    }

    public Trailer(int movieId, String id, String key, String name, String site, int size, String type) {
        this.movieId = movieId;
        this.id = id;
        this.key = key;
        this.name = name;
        this.site = site;
        this.size = size;
        this.type = type;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
