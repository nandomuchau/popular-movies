package com.muchau.popularmovies.utilities;

import com.muchau.popularmovies.model.Movie;
import com.muchau.popularmovies.model.Review;
import com.muchau.popularmovies.model.Trailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 5/9/2018.
 */
public class JsonUtils {

    // Movie list
    private static final String RESULTS = "results";
    private static final String VOTE_COUNT = "vote_count";
    private static final String ID = "id";
    private static final String VIDEO = "video";
    private static final String VOTE_AVERAGE = "vote_average";
    private static final String TITLE = "title";
    private static final String POPULARITY = "popularity";
    private static final String POSTER_PATH = "poster_path";
    private static final String ORIGINAL_LANGUAGE = "original_language";
    private static final String ORIGINAL_TITLE = "original_title";
    private static final String GENRE_IDS = "genre_ids";
    private static final String BACKDROP_PATH = "backdrop_path";
    private static final String ADULT = "adult";
    private static final String OVERVIEW = "overview";
    private static final String RELEASE_DATE = "release_date";

    // Trailer list
    private static final String KEY = "key";
    private static final String NAME = "name";
    private static final String SITE = "site";
    private static final String SIZE = "size";
    private static final String TYPE = "type";

    // Review list
    private static final String AUTHOR = "author";
    private static final String CONTENT = "content";
    private static final String URL = "url";

    public static ArrayList<Movie> getMovieValuesFromJson(String mainJsonStr)
            throws JSONException {

        ArrayList<Movie> movieList = new ArrayList<>();
        Movie movie;

        JSONObject mainJson = new JSONObject(mainJsonStr);
        JSONArray jsonMoviesArray = mainJson.getJSONArray(RESULTS);

        for (int i = 0; i < jsonMoviesArray.length(); i++) {
            movie = new Movie();

            JSONObject movieObj = jsonMoviesArray.getJSONObject(i);

            movie.setVoteCount(movieObj.getInt(VOTE_COUNT));
            movie.setMovieId(movieObj.getInt(ID));
            movie.setVideo(movieObj.getBoolean(VIDEO));
            movie.setVoteAverage(movieObj.getInt(VOTE_AVERAGE));
            movie.setTitle(movieObj.getString(TITLE));
            movie.setPopularity(movieObj.getLong(POPULARITY));
            movie.setPosterPath(movieObj.getString(POSTER_PATH));
            movie.setOriginalLanguage(movieObj.getString(ORIGINAL_LANGUAGE));
            movie.setOriginalTitle(movieObj.getString(ORIGINAL_TITLE));
            movie.setBackdropPath(movieObj.getString(BACKDROP_PATH));
            movie.setAdult(movieObj.getBoolean(ADULT));
            movie.setOverview(movieObj.getString(OVERVIEW));
            movie.setReleaseDate(movieObj.getString(RELEASE_DATE));

            movieList.add(movie);
        }

        return movieList;
    }

    public static ArrayList<Trailer> getTrailerValuesFromJson(String mainJsonStr)
            throws JSONException {

        ArrayList<Trailer> trailerList = new ArrayList<>();
        Trailer trailer;

        JSONObject mainJson = new JSONObject(mainJsonStr);
        int movieID = mainJson.optInt(ID);
        JSONArray jsonMoviesArray = mainJson.getJSONArray(RESULTS);

        for (int i = 0; i < jsonMoviesArray.length(); i++) {
            trailer = new Trailer();

            JSONObject movieObj = jsonMoviesArray.getJSONObject(i);

            trailer.setMovieId(movieID);
            trailer.setId(movieObj.getString(ID));
            trailer.setKey(movieObj.getString(KEY));
            trailer.setName(movieObj.getString(NAME));
            trailer.setSize(movieObj.getInt(SIZE));
            trailer.setSite(movieObj.getString(SITE));
            trailer.setType(movieObj.getString(TYPE));

            trailerList.add(trailer);
        }

        return trailerList;
    }

    public static ArrayList<Review> getReviewValuesFromJson(String mainJsonStr)
            throws JSONException {

        ArrayList<Review> reviewList = new ArrayList<>();
        Review review;

        JSONObject mainJson = new JSONObject(mainJsonStr);
        JSONArray jsonMoviesArray = mainJson.getJSONArray(RESULTS);

        for (int i = 0; i < jsonMoviesArray.length(); i++) {
            review = new Review();

            JSONObject movieObj = jsonMoviesArray.getJSONObject(i);

            review.setId(movieObj.getString(ID));
            review.setAuthor(movieObj.getString(AUTHOR));
            review.setContent(movieObj.getString(CONTENT));
            review.setUrl(movieObj.getString(URL));

            reviewList.add(review);
        }

        return reviewList;
    }
}
