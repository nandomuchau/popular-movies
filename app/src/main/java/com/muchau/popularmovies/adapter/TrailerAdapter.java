package com.muchau.popularmovies.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.muchau.popularmovies.R;
import com.muchau.popularmovies.model.Trailer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 6/20/2018.
 */
public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.ViewHolder> {

    private static final String TAG = TrailerAdapter.class.getSimpleName();

    private ArrayList<Trailer> mData;

    private LayoutInflater mInflater;

    private Context mContext;

    private static final String YOUTUBE_URL = "https://www.youtube.com/watch?v=";

    private static final String YOUTUBE_PIC_URL = "https://img.youtube.com/vi/";

    private static final String YOUTUBE_PIC_SIZE = "/1.jpg";

    // data is passed into the constructor
    public TrailerAdapter(Context context, ArrayList<Trailer> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.trailer_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Trailer trailer = mData.get(position);
        String pictureUrl = YOUTUBE_PIC_URL + trailer.getKey() + YOUTUBE_PIC_SIZE;

        Picasso.with(mContext)
                .load(pictureUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .into(holder.trailerImage);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView trailerImage;

        ViewHolder(View itemView) {
            super(itemView);
            trailerImage = itemView.findViewById(R.id.trailer_imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Trailer trailer = getItem(getAdapterPosition());
            openYoutubeVideo(trailer.getKey());
        }
    }

    public void openYoutubeVideo(String id){
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(YOUTUBE_URL + id));
        try {
            mContext.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            mContext.startActivity(webIntent);
        }
    }

    public void updateTrailers(ArrayList<Trailer> trailers) {
        this.mData = trailers;
    }

    private Trailer getItem(int id) {
        return mData!= null ? mData.get(id) : null;
    }
}