package com.muchau.popularmovies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muchau.popularmovies.R;
import com.muchau.popularmovies.model.Review;

import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 6/21/2018.
 */
public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    private ArrayList<Review> mData;

    private LayoutInflater mInflater;

    private Context mContext;

    // data is passed into the constructor
    public ReviewAdapter(Context context, ArrayList<Review> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.review_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Review review = mData.get(position);
        holder.content.setText(review.getContent());
        holder.author.setText(review.getAuthor());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView content;
        TextView author;

        ViewHolder(View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.content_textView);
            author = itemView.findViewById(R.id.author_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public void updateReviews(ArrayList<Review> reviews) {
        this.mData = reviews;
    }

    private Review getItem(int id) {
        return mData!= null ? mData.get(id) : null;
    }
}